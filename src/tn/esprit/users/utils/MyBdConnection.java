/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.users.utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nada
 */
public class MyBdConnection {
   static String url = "jdbc:mysql:"+ "//localhost:3306/base";
   static String login ="root";
   static String pwd ="";
   static Connection mycon;
   static MyBdConnection instanceBD;
  
    public static void MyBdConnection() {
      try {
          //get a connection to database
          mycon = DriverManager.getConnection(url, login, pwd);
          System.out.println("connexion etablite !!");
      } catch (SQLException ex) {
          System.out.println(ex.getMessage());
      }
        
    }
    
    public static MyBdConnection  getInstanceBD(){
        if(instanceBD == null)
            instanceBD = new MyBdConnection();
        return instanceBD;
        
    }
    
    public Connection getConnection(){
        return mycon;
    }
    
    
    
      
    
}
