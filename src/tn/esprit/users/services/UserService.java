/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.users.services;
import tn.esprit.users.utils.MyBdConnection;
import tn.esprit.users.entites.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
/**
 *
 * @author Nada
 */
public class UserService implements IUserService {
    Connection mycon;
    public UserService() {
    mycon = MyBdConnection.getInstanceBD().getConnection();

    }
    @Override
    public  boolean insererUser(User u)  {
        try {
       String req1 = "INSERT INTO `users`( `role`, `nom`, `prenom`, `mail`, `tel`, `adresse`, `mot_pass`) VALUES (?,?,?,?,?,?,?)";
       Statement ste= mycon.createStatement();
                    PreparedStatement ps = mycon.prepareStatement(req1);
                    
                    ps.setString(1, u.getRole());
                    ps.setString(2, u.getNom());
                    ps.setString(3, u.getPrenom());
                    ps.setString(4, u.getMail());
                    ps.setString(5, u.getTel());
                    ps.setString(6, u.getAdresse());
                    ps.setString(7, u.getMot_pass());
                    
                    
                    ps.executeUpdate();

       System.out.println(req1);
       
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

      
             
       return true;
  }

    /**
     *
     * @param u
     * @return
     * @throws SQLException
     */
    @Override
    public  boolean modifierUser(User u) throws SQLException {
        
        
        return false;
    }

    /**
     *
     * @param u
     * @return
     * @throws SQLException
     */
    @Override
    public  boolean supprimerUser(User u) throws SQLException {
        
     String req = "delete from users where "
                + "id= ? ";
        
       PreparedStatement ps = mycon.prepareStatement(req);
       ps.setInt(1, u.getId());
       ps.executeUpdate();
              return true;
  
    }
    @Override
    public User retournerID(String pass,String mail) throws SQLException
    {
      String url = "jdbc:mysql:"+ "//localhost:3306/base";
    String login ="root";
    String pwd ="";
    Connection vv;
   
  
   
          //get a connection to database
          vv = DriverManager.getConnection(url, login, pwd);
          System.out.println("connexion etablite !!");
     
          String req1 = "select * from users WHERE mail='"+mail+"' and "+"mot_pass='"+pass+"'";
        System.out.println(req1);
       Statement ste = vv.createStatement();
      ResultSet res= ste.executeQuery(req1);
   /*
       while (res.next()) {
                       
            System.out.println("user est "+
                    res.getInt("id")
             +"son role est "+ res.getString("role")
            +"son nom est  "+ res.getString("nom") +"son prenom est"+ res.getString("role")
            +"son mail est "+ res.getString("mail") +"son adresse est "+ res.getString("adresse")
            +"son tel est "+ res.getString("tel") +"son mot de pass est "+ res.getString("mot_pass"));
            
           
        }
      */
       if(res.next())  {
            User user = new User(res.getInt("id"), res.getString("role") , "req1", "req1", "req1", "req1", "req1", "req1");
           return user;
       }
       else return null;
     
    }
    
    public String retournerRole(String mail)
    {
         try {
      String url = "jdbc:mysql:"+ "//localhost:3306/base";
    String login ="root";
    String pwd ="";
    Connection vv;
   
  
   
       
            //get a connection to database
            vv = DriverManager.getConnection(url, login, pwd);
            System.out.println("connexion etablite !!");
     
          String req1 = "select * from users WHERE mail='"+mail+"'";
       
       Statement ste = vv.createStatement();
      ResultSet res= ste.executeQuery(req1);
      String role ;
            
        while (res.next())
        {
        role=res.getString("role");
            System.out.println(role);
        return role ;
        
        }
        return "Il y a pas" ;
        } catch (SQLException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    return "Il y a pas" ;
    } 
            

    @Override
    public ObservableList<User> AfficherUsers() throws SQLException {
        
        ObservableList<User> tab = FXCollections.observableArrayList();
        
          String req1 = "select * from users";
       
       Statement ste = mycon.createStatement();
      ResultSet res= ste.executeQuery(req1);
        while (res.next()) {
                       
            System.out.println("user est "+
                    res.getInt("id")
             +"son role est "+ res.getString("role")
            +"son nom est  "+ res.getString("nom") +"son prenom est"+ res.getString("role")
            +"son mail est "+ res.getString("mail") +"son adresse est "+ res.getString("adresse")
            +"son tel est "+ res.getString("tel") +"son mot de pass est "+ res.getString("mot_pass"));
            
            User user = new User(res.getInt("id"), res.getString("role") , "req1", "req1", "req1", "req1", "req1", "req1");
            tab.add(user);
        }
        return tab;
    }


    
}
