/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.users.services;
import tn.esprit.users.entites.User;
import java.sql.SQLException;
import javafx.collections.ObservableList;

/**
 *
 * @author Nada
 */
public interface IUserService {
    public boolean insererUser(User u) ;
    public  boolean modifierUser(User u) throws SQLException;
        public  boolean supprimerUser(User u) throws SQLException ;
           public ObservableList<User> AfficherUsers() throws SQLException;
            public User retournerID(String pass,String mail) throws SQLException ;
             public String retournerRole(String mail);
}
