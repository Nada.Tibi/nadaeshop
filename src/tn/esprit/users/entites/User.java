/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.esprit.users.entites;

/**
 *
 * @author Nada
 */ 
public class User {
    int id;
    String role;
    String nom;
    String prenom;
    String mail;
    String tel;
    String adresse;
    String mot_pass;

    public User() {
    }

    public User(int id, String role, String nom, String prenom, String mail, String tel, String adresse, String mot_pass) {
        this.id = id;
        this.role = role;
        this.nom = nom;
        this.prenom = prenom;
        this.mail = mail;
        this.tel = tel;
        this.adresse = adresse;
        this.mot_pass = mot_pass;
    }

    public int getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMail() {
        return mail;
    }

    public String getTel() {
        return tel;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getMot_pass() {
        return mot_pass;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setMot_pass(String mot_pass) {
        this.mot_pass = mot_pass;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", role=" + role + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", tel=" + tel + ", adresse=" + adresse + ", mot_pass=" + mot_pass + '}';
    }
    
    
}
