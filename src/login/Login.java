/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.sql.SQLException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import tn.esprit.users.services.UserService;
import static tn.esprit.users.utils.MyBdConnection.MyBdConnection;

/**
 *
 * @author Nada
 */
public class Login extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
       
        
          
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {

       /* MyBdConnection();
        String ss="aymen"; 
        UserService cc = new UserService() ;
        cc.retournerRole(ss); */
        launch(args);
        
    
    
    }
    
}
