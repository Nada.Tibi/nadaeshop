/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import A.registrationformapplication.RegistrationFormApplication;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.stage.Window;
import tn.esprit.users.entites.User;
import tn.esprit.users.services.UserService;
import static tn.esprit.users.utils.MyBdConnection.MyBdConnection;

/**
 *
 * @author Nada
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private JFXPasswordField password;

    @FXML
    private JFXTextField mail;

    @FXML
    private JFXButton login;

    @FXML
    private JFXButton signup;

    @FXML
    void makeLogin(ActionEvent event) throws SQLException, IOException, Exception {
        String Email= mail.getText();
        String pass = password.getText();
        Window owner = signup.getScene().getWindow();
        if(mail.getText().isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter your email id");
            return;
        }
        if(password.getText().isEmpty()) {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter your password");
            return;
        }
        
        if(VerifierLongChaine(password.getText(),"pass", 3, 12)!="valide") {
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
               VerifierLongChaine(password.getText(),"pass", 3, 12)     );
            return;
        }
        
        MyBdConnection();
        UserService US = new UserService();
        User user = new User();
       
    user = US.retournerID(password.getText(),mail.getText());
    
         if(user!=null) {System.out.println("trouvé");
         StaticVars.A=user;
     
         Parent root = FXMLLoader.load(getClass().getResource("CompteFXML.fxml"));
    Scene newScene= new Scene(root);
    Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
       window.setScene(newScene);
       window.show();

         }
         //else System.out.println("non trouvé");
        else AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                   "Veuillez s'inscrire SVP , cliquez sur sign up et remplissez le formulaire");
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    public String VerifierLongChaine(String chaine, String champ, int nbr_carac_min, int nbr_carac_max) {
        if (chaine.length() <= nbr_carac_max && chaine.length() >= nbr_carac_min) {
            return "valide";
        }
        return "le champ " + champ + " doit etre entre " + nbr_carac_min + " et " + nbr_carac_max + " caractères !";
    }

    @FXML
    private void makeSignUp(ActionEvent event) throws Exception {
        RegistrationFormApplication cc = new RegistrationFormApplication() ;
        Stage stage= new Stage () ;
    cc.start(stage);
    }
}
